package main.java;

import java.util.ArrayList;
import java.util.List;

public class ContactService implements IContactService{

    private List<Contact> contacts = new ArrayList<>();

    @Override
    public Contact creerContact(String nom) throws ContactException {
        if(nom == null || nom.length() < 3 || nom.length() > 40){
            throw new ContactException("La taille du nom n'est pas correct");
        }

        if(contacts.contains(new Contact(nom))){
            throw new ContactException("Le nom est déjà utilisé");
        }


        Contact contact = new Contact(nom);
        contacts.add(contact);
        return contact;
    }
}
