import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContactServiceTest {

    ContactService service = new ContactService();

    @Test
    public void shouldFailNull(){
        Assertions.assertThrows(ContactException.class,
                () -> service.creerContact(null));
    }

    @Test
    public void testCreerContactNomNull() throws ContactException {
        service.creerContact(null);
    }

    @Test
    public void testCreerContactNomVide() throws ContactException {
        service.creerContact("");
    }

    @Test
    public void testCreerContactNomTropCourt() throws ContactException {
        service.creerContact("AB");
    }

    @Test
    public void testCreerContactNomTropLong() throws ContactException {
        service.creerContact("ABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBAABBBBBBBBA");
    }
}